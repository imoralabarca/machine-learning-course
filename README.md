# Machine Learning Course Spring 2022



## Machine Learning Assignments 


## Description
This repository was made to showcase various Machine Learning assignments that were developed through a Masters' Machine Learning course. Each assignment summary is provided below.

#### **Assignment 1** 
 - Predictive model for Boston house prices using Linear regression 
 - Given a dataset with features that are computed from a digitized image of a fine needle aspirate (FNA) of a breast mass, which describes characteristics of the cell nuclei present in the image, predict whether the patients are diagnosed as Malignant (M) or Benign (B)
#### **Assignment 2**
 - Propensity model to evaluate the effectiveness of telemarketing campaigns, i.e. whether the customer subscribed to the term deposit, along with expected profits based on recommended actions to take
#### **Assignment 3**
 - Apply KMeans for image compression, by reducing the number of colours that occur in an image to only those that are most common in that image
 - Image classifier
 - Recommender system (content-based and collaborative filtering) considering movie ratings
#### **Assignment 4** 
 -  Deep neural network on the CIFAR10 image dataset: neural network, convolutional neural network and transfer learning based neural network

## Authors and acknowledgment
Mora Labarca, Isabel : 48516@novasbe.pt

## License
GNU GENERAL PUBLIC LICENSE

## Project status
Completed
